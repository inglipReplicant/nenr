extern crate evo;
extern crate rand;
extern crate rand_distr;

use crate::util::sigmoid;

use evo::{
    fitness::FitnessFunction,
    ops::{CrossoverOp, MutationOp},
    population::Population,
    real::*,
    unit::Unit,
};
use rand::{thread_rng, Rng};
use rand_distr::StandardNormal;

pub type EvolnetExample = (Vec<f64>, Vec<f64>);

#[derive(Clone, Debug)]
pub struct EvolNet {
    arch: Vec<usize>,
    neurons: Vec<f64>,
    params: Vec<f64>,
    dataset: Vec<EvolnetExample>,
}

impl EvolNet {
    pub fn new(architecture: Vec<usize>, dataset: Vec<EvolnetExample>) -> EvolNet {
        let n_neurons = architecture.iter().fold(0, |acc, x| acc + x);
        let n_params = count_params(&architecture);
        EvolNet {
            arch: architecture,
            neurons: vec![0.0; n_neurons],
            params: vec![0.0; n_params],
            dataset: dataset,
        }
    }

    pub fn predict(&mut self, input: &Vec<f64>) -> Vec<f64> {
        //println!("Starting prediction...");
        let mut neuron_offset = 0;
        let mut params_offset = 0;
        let last = self.arch.len() - 1;

        // Set input layer neurons to input values
        for i in neuron_offset..self.arch[0] {
            self.neurons[i] = input[i];
        }
        neuron_offset += self.arch[0];

        //println!("Done setting input layer neurons!");

        // Type 1 layer
        for n in neuron_offset..(self.arch[1] + neuron_offset) {
            self.neurons[n] = self.type_1_output(n, neuron_offset);
        }
        neuron_offset += self.arch[1];
        params_offset += self.arch[0] * self.arch[1] * 2;

        // Remaining layers - Type 2
        for l in 2..self.arch.len() {
            for n in neuron_offset..self.arch[l] + neuron_offset {
                let net = self.net_output(params_offset, l, n, neuron_offset);
                self.neurons[n] = sigmoid(net);
            }
            neuron_offset += self.arch[l];
            params_offset += self.arch[l] * (self.arch[l - 1] + 1);
        }
        neuron_offset -= self.arch[last];

        Vec::from(&self.neurons[neuron_offset..])
    }

    fn net_output(
        &self,
        param_offset: usize,
        layer: usize,
        neuron: usize,
        neuron_offset: usize,
    ) -> f64 {
        //println!("Calculating net output...");
        let mut net = 0.0;
        let curr = neuron - neuron_offset;
        let prev_layer = layer - 1;
        let k = neuron_offset - self.arch[prev_layer];

        for i in 0..self.arch[prev_layer] {
            net +=
                self.params[param_offset + self.arch[prev_layer] * curr + i] * self.neurons[k + i];
        }
        net += self.params[param_offset + self.arch[prev_layer] * self.arch[layer] + curr];
        //println!("Calculated net output!");

        net
    }

    fn type_1_output(&self, neuron: usize, offset: usize) -> f64 {
        //println!("Calculating type 1 output...");
        let mut total = 0.0;
        let curr = neuron - offset;
        for j in 0..self.arch[0] {
            let param_ind = curr * self.arch[0] * 2 + j * 2;
            total +=
                (self.neurons[j] - self.params[param_ind]).abs() / self.params[param_ind + 1].abs();
        }
        //println!("Returning type 1 output!");
        1.0 / (1.0 + total)
    }

    pub fn total_mse(&mut self, normalize: bool) -> f64 {
        let mut mse = 0.0;
        for example in self.dataset.clone().iter() {
            let predicted = self.predict(&example.0);
            let actual = &example.1;
            for i in 0..predicted.len() {
                mse += (actual[i] - predicted[i]).powi(2);
            }
        }

        if normalize {
            mse / self.dataset.len() as f64
        } else {
            mse
        }
    }

    pub fn param_len(&self) -> usize {
        self.params.len()
    }

    pub fn set_params(&mut self, new_params: &Vec<f64>) {
        self.params = new_params.clone();
    }

    pub fn get_params(&self) -> &Vec<f64> {
        &self.params
    }

    pub fn get_params_mut(&mut self) -> &mut Vec<f64> {
        &mut self.params
    }

    pub fn get_neurons(&self) -> &Vec<f64> {
        &self.neurons
    }

    pub fn get_neurons_mut(&mut self) -> &mut Vec<f64> {
        &mut self.neurons
    }

    pub fn update_evaluate_real_pop(&mut self, pop: &mut Population<RealUnit>) {
        for unit in pop.units_mut() {
            self.set_params(unit.get_chromo());
            unit.update_fitness(self.total_mse(true));
        }
    }
}

fn count_params(arch: &Vec<usize>) -> usize {
    let mut counter = 0;
    counter += 2 * arch[0] * arch[1];
    for i in 1..arch.len() - 1 {
        counter += (arch[i] + 1) * arch[i + 1];
    }
    counter
}

impl FitnessFunction<RealUnit, f64> for EvolNet {
    fn fitness_of(&mut self, x: &RealUnit) -> f64 {
        self.set_params(x.get_chromo());
        self.total_mse(true)
    }

    fn fitness_min(&self) -> f64 {
        std::f64::MIN
    }

    fn fitness_max(&self) -> f64 {
        std::f64::MAX
    }
}

pub struct CupicMutator {
    sig_1: f64,
    sig_2: f64,
    sig_3: f64,

    pmut_1: f64,
    pmut_2: f64,
    pmut_3: f64,

    tmut: Vec<f64>,
    tsum: f64,
}

impl CupicMutator {
    pub fn new() -> CupicMutator {
        CupicMutator {
            sig_1: 0.1,
            sig_2: 0.5,
            sig_3: 1.0,
            pmut_1: 0.1,
            pmut_2: 0.05,
            pmut_3: 0.01,
            tmut: vec![6.0, 2.0, 2.0],
            tsum: 8.0,
        }
    }

    pub fn mutate_one(&self, child: &mut Vec<f64>, sig: f64, p: f64, mode: bool) {
        let mut rng = thread_rng();
        for i in 0..child.len() {
            if thread_rng().gen_range(0.0, 1.0) < p {
                let rnum: f64 = rng.sample(StandardNormal);
                child[i] = if mode {
                    rnum * sig
                } else {
                    child[i] + rnum * sig
                };
            }
        }
    }
}

impl MutationOp<RealUnit> for CupicMutator {
    fn mutate<R>(&self, unit: RealUnit, rng: &mut R) -> RealUnit
    where
        R: Rng + Sized,
    {
        let sum = self.tsum;
        let probs = vec![self.tmut[0] / sum, self.tmut[1] / sum, self.tmut[2] / sum];

        let chosen_p = rng.gen_range(0.0, 1.0);
        let mut cumulative = 0.0;
        let mut chosen = 0;

        for (i, p) in (0..=2).zip(probs.iter()) {
            cumulative += p;
            if chosen_p <= cumulative {
                chosen = i;
            }
        }

        let mut child_vec = unit.get_chromo().clone();
        match chosen {
            0 => self.mutate_one(&mut child_vec, self.sig_1, self.pmut_1, false),
            1 => self.mutate_one(&mut child_vec, self.sig_2, self.pmut_2, false),
            _ => self.mutate_one(&mut child_vec, self.sig_3, self.pmut_3, true),
        };
        RealUnit::from_vec(child_vec)
    }
}

pub struct CupicCrossover {
    blxa: BlxAlpha,
    disc: DiscreteCrossover,
    arit: ArithmeticCrossover,
}

impl CupicCrossover {
    pub fn new(alpha: f64, bias: f64) -> CupicCrossover {
        CupicCrossover {
            blxa: BlxAlpha { alpha: alpha },
            disc: DiscreteCrossover { bias: bias },
            arit: ArithmeticCrossover { lambda: 0.5 },
        }
    }
}

impl CrossoverOp<RealUnit> for CupicCrossover {
    fn crossover<R>(&self, parents: Vec<RealUnit>, rng: &mut R) -> Vec<RealUnit>
    where
        R: Rng + Sized,
    {
        match rng.gen_range(0, 3) {
            0 => self.blxa.crossover(parents, rng),
            1 => self.disc.crossover(parents, rng),
            _ => self.arit.crossover(parents, rng),
        }
    }
}
