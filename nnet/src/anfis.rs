extern crate rand;

use crate::util::*;
use rand::seq::SliceRandom;
use rand::thread_rng;

pub type AnfisExample = (Vec<f64>, f64);

pub struct ANFIS {
    dataset: Vec<AnfisExample>,
    k: usize,

    pub a: Vec<f64>,
    pub c: Vec<f64>,
    pub b: Vec<f64>,
    pub d: Vec<f64>,
    pub p: Vec<f64>,
    pub q: Vec<f64>,
    pub r: Vec<f64>,

    a_grad: Vec<f64>,
    c_grad: Vec<f64>,
    b_grad: Vec<f64>,
    d_grad: Vec<f64>,
    p_grad: Vec<f64>,
    q_grad: Vec<f64>,
    r_grad: Vec<f64>,

    sigm_a: Vec<f64>,
    sigm_b: Vec<f64>,
    ws: Vec<f64>,
    ws_norm: Vec<f64>,
    fs: Vec<f64>,
}

impl ANFIS {
    pub fn new(dataset: Vec<AnfisExample>, k: usize) -> ANFIS {
        ANFIS {
            dataset: dataset,
            k: k,

            a: create_vec_rand(k, 1.0),
            b: create_vec_rand(k, 1.0),
            c: create_vec_rand(k, 1.0),
            d: create_vec_rand(k, 1.0),
            p: create_vec_rand(k, 1.0),
            q: create_vec_rand(k, 1.0),
            r: create_vec_rand(k, 1.0),

            a_grad: create_vec_empty(k),
            c_grad: create_vec_empty(k),
            b_grad: create_vec_empty(k),
            d_grad: create_vec_empty(k),
            p_grad: create_vec_empty(k),
            q_grad: create_vec_empty(k),
            r_grad: create_vec_empty(k),

            sigm_a: create_vec_empty(k),
            sigm_b: create_vec_empty(k),
            ws: create_vec_empty(k),
            ws_norm: create_vec_empty(k),
            fs: create_vec_empty(k),
        }
    }

    pub fn epoch(
        &mut self,
        batch_size: Option<usize>,
        learning_rate: f64,
        err_trace: bool,
    ) -> Option<f64> {
        let batch_size = batch_size.unwrap_or(1);

        self.dataset.shuffle(&mut thread_rng());

        let epoch_error = self
            .dataset
            .clone()
            .chunks(batch_size)
            .map(|batch| self.process_batch(&batch, learning_rate))
            .sum();

        match err_trace {
            true => Some(epoch_error),
            false => None,
        }
    }

    fn process_batch(&mut self, batch: &[AnfisExample], learning_rate: f64) -> f64 {
        self.reset_gradients();
        let batch_error = batch
            .iter()
            .map(|example| self.update_gradient(example))
            .sum();

        self.apply_gradients(learning_rate, batch.len());
        batch_error
    }

    // Isto izgleda ok...
    fn update_gradient(&mut self, example: &AnfisExample) -> f64 {
        let x = example.0[0];
        let y = example.0[1];
        let t = example.1;
        let predicted = self.forward_propagate(example);
        let err_derived = predicted - t;

        let wsum = self.ws.iter().fold(0.0, |acc, w| acc + w);
        let wsum_squared = wsum.powi(2);
        let mut fun_sum = 0.0;
        for i in 0..self.k {
            fun_sum += self.fs[i] * self.ws[i];
        }

        for i in 0..self.k {
            self.p_grad[i] += err_derived * self.ws_norm[i] * x;
            self.q_grad[i] += err_derived * self.ws_norm[i] * y;
            self.r_grad[i] += err_derived * self.ws_norm[i];

            let exp = (self.b[i] * (x - self.a[i])).exp();
            let a_grad = self.sigm_b[i] * self.b[i] * exp / (1.0 + exp).powi(2);
            let b_grad = self.sigm_b[i] * (self.a[i] - x) * exp / (1.0 + exp).powi(2);
            self.a_grad[i] += err_derived * a_grad * (self.fs[i] / wsum - fun_sum / wsum_squared);
            self.b_grad[i] += err_derived * b_grad * (self.fs[i] / wsum - fun_sum / wsum_squared);

            let exp = (self.d[i] * (y - self.c[i])).exp();
            let c_grad = self.sigm_a[i] * self.d[i] * exp / (1.0 + exp).powi(2);
            let d_grad = self.sigm_a[i] * (self.c[i] - y) * exp / (1.0 + exp).powi(2);
            self.c_grad[i] += err_derived * c_grad * (self.fs[i] / wsum - fun_sum / wsum_squared);
            self.d_grad[i] += err_derived * d_grad * (self.fs[i] / wsum - fun_sum / wsum_squared);
        }

        0.5 * (predicted - example.1).powi(2)
    }

    pub fn forward_propagate(&mut self, example: &AnfisExample) -> f64 {
        let x = example.0[0];
        let y = example.0[1];

        for i in 0..self.k {
            self.sigm_a[i] = sigmoid_param(x, self.a[i], self.b[i]);
            self.sigm_b[i] = sigmoid_param(y, self.c[i], self.d[i]);
            self.ws[i] = self.sigm_a[i] * self.sigm_b[i];
        }

        let wsum = self.ws.iter().fold(0.0, |acc, w| acc + w);
        let mut predicted = 0.0;
        for i in 0..self.k {
            self.ws_norm[i] = self.ws[i] / wsum;
            self.fs[i] = self.p[i] * x + self.q[i] * y + self.r[i];
            predicted += self.ws_norm[i] * self.fs[i];
        }

        predicted
    }

    // Ovo je ok
    fn apply_gradients(&mut self, learning_rate: f64, batch_size: usize) {
        let batch_size = batch_size as f64;
        for i in 0..self.k {
            self.a[i] -= learning_rate * self.a_grad[i] / batch_size;
            self.b[i] -= learning_rate * self.b_grad[i] / batch_size;
            self.c[i] -= learning_rate * self.c_grad[i] / batch_size;
            self.d[i] -= learning_rate * self.d_grad[i] / batch_size;
            self.p[i] -= learning_rate * self.p_grad[i] / batch_size;
            self.q[i] -= learning_rate * self.q_grad[i] / batch_size;
            self.r[i] -= learning_rate * self.r_grad[i] / batch_size;
        }
    }

    //ovo je ok!!
    fn reset_gradients(&mut self) {
        for i in 0..self.k {
            self.a_grad[i] = 0.0;
            self.b_grad[i] = 0.0;
            self.c_grad[i] = 0.0;
            self.d_grad[i] = 0.0;
            self.p_grad[i] = 0.0;
            self.q_grad[i] = 0.0;
            self.r_grad[i] = 0.0;
        }
    }

    pub fn total_mse(&mut self) -> f64 {
        let total = self.dataset.clone().iter().fold(0.0, |acc, ex| {
            acc + (ex.1 - self.forward_propagate(&ex)).powi(2)
        });
        0.5 * total / self.dataset.len() as f64
    }
}
