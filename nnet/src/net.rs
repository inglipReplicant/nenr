extern crate linalg;

use crate::layer::Layer;
use crate::util::*;
use linalg::matrix::{Matrix, RealMat};

pub struct NeuralNet<'a> {
    pub layers: Vec<&'a mut dyn Layer>,
}

impl<'a> NeuralNet<'a> {
    pub fn new(layers: Vec<&'a mut dyn Layer>) -> NeuralNet<'a> {
        NeuralNet { layers: layers }
    }

    pub fn fit(
        &mut self,
        examples: &[RealMat],
        labels: &[RealMat],
        batch_size: Option<usize>,
        _precision: f64,
        max_epochs: usize,
        learning_rate: f64,
    ) {
        let mut epoch = 0;
        let batch_size = batch_size.unwrap_or(examples.len());

        while epoch < max_epochs {
            let (xs, ys) = shuffle_dataset(examples, labels);
            //println!("{:?}\n{:?}", xs, ys);

            xs.chunks(batch_size)
                .zip(ys.chunks(batch_size))
                .for_each(|(x, y)| self.process_chunk(x, y, learning_rate, batch_size));

            epoch += 1;
        }
    }

    fn process_chunk(
        &mut self,
        xs: &[&RealMat],
        ys: &[&RealMat],
        learning_rate: f64,
        batch_size: usize,
    ) {
        let batch_len = if xs.len() < batch_size {
            xs.len()
        } else {
            batch_size
        };
        for i in 0..batch_len {
            println!("\nDoing example {}, {}...", xs[i], ys[i]);

            let output = self.forward_prop_once(xs[i]);
            let mut grad = output.sub(ys[i]).expect("Sub error in chunk");
            println!("Starting grad: {}", grad);
            for j in (0..self.layers.len()).rev() {
                grad = self.layers.get_mut(j).unwrap().backward_pass(&grad);
            }

            for j in 0..self.layers.len() {
                self.layers.get_mut(j).unwrap().update_weight(learning_rate);
            }
        }
    }

    pub fn forward_prop_once(&mut self, x: &RealMat) -> RealMat {
        let mut output = x.clone();
        for i in 0..self.layers.len() {
            output = self.layers.get_mut(i).unwrap().forward_pass(&output);
        }
        return output;
    }
}
