use crate::{
    fitness::{Fitness, FitnessFunction},
    ops::*,
    population::Population,
    unit::Unit,
};

use std::marker::PhantomData;

#[derive(Clone, Debug, PartialEq)]
pub struct GeneticAlgorithm<G, F, E, S, C, M, R>
where
    G: Unit,
    F: Fitness,
    E: FitnessFunction<G, F>,
    S: SelectionOp<G, F>,
    C: CrossoverOp<G>,
    M: MutationOp<G>,
    R: ReinsertionOp<G, F>,
{
    _f: PhantomData<F>,
    evaluator: E,
    selector: S,
    breeder: C,
    mutator: M,
    reinserter: R,
    min_pop_size: usize,
    init_pop: Population<G>,
    population: Population<G>,
}

impl<G, F, E, S, C, M, R> GeneticAlgorithm<G, F, E, S, C, M, R>
where
    G: Unit,
    F: Fitness,
    E: FitnessFunction<G, F>,
    S: SelectionOp<G, F>,
    C: CrossoverOp<G>,
    M: MutationOp<G>,
    R: ReinsertionOp<G, F>,
{
    pub fn new(
        min_pop_size: usize,
        evaluator: E,
        selector: S,
        breeder: C,
        mutator: M,
        reinserter: R,
        init_pop: Population<G>,
    ) -> GeneticAlgorithm<G, F, E, S, C, M, R> {
        GeneticAlgorithm {
            _f: PhantomData,
            evaluator: evaluator,
            selector: selector,
            breeder: breeder,
            mutator: mutator,
            reinserter: reinserter,
            min_pop_size: min_pop_size,
            init_pop: init_pop.clone(),
            population: init_pop,
        }
    }

    pub fn evaluator(&self) -> &E {
        &self.evaluator
    }

    pub fn selector(&self) -> &S {
        &self.selector
    }

    pub fn breeder(&self) -> &C {
        &self.breeder
    }

    pub fn mutator(&self) -> &M {
        &self.mutator
    }

    pub fn reinserter(&self) -> &R {
        &self.reinserter
    }

    pub fn min_pop_size(&self) -> usize {
        self.min_pop_size
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct State<G>
where
    G: Unit,
{
    /// The evaluated population of the current generation.
    pub evaluated_population: Population<G>,
    /// Best solution of this generation.
    pub best_solution: G,
}
