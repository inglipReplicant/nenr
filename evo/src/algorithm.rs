use std::fmt::Debug;

use rand::rngs::ThreadRng;

pub trait Algorithm {
    type Output: Clone + Debug + PartialEq;

    fn next(&mut self, iteration: usize, rng: &mut ThreadRng) -> Self::Output;
}
