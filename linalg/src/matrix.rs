use crate::error::MathError;
use crate::vector::{RealVec, Vector};
use crate::PRECISION;

use std::fmt;
use std::fs::File;
use std::io::prelude::*;
use std::ops::{Add, Mul, Sub};

type Result<T> = std::result::Result<T, MathError>;

pub trait Matrix {
    fn add(&self, other: &Self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn add_inplace(&mut self, other: &Self) -> Result<()>;

    fn sub(&self, other: &Self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn sub_inplace(&mut self, other: &Self) -> Result<()>;

    fn mul(&self, other: &Self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn transpose(&self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn from_vec_into_col(v: &impl Vector) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn from_vec_into_row(v: &impl Vector) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn values(&self) -> &Vec<Vec<f64>>;

    fn values_mut(&mut self) -> &mut Vec<Vec<f64>>;

    fn dim_cols(&self) -> usize;

    fn dim_rows(&self) -> usize;

    fn is_square(&self) -> bool;

    fn get_elem(&self, row: usize, col: usize) -> Result<f64>;

    fn set_elem(&mut self, row: usize, col: usize, new: f64) -> Result<()>;

    fn add_elem(&mut self, row: usize, col: usize, num: f64) -> Result<()>;

    fn sub_elem(&mut self, row: usize, col: usize, num: f64) -> Result<()>;

    fn mul_elem(&mut self, row: usize, col: usize, coef: f64) -> Result<()>;

    fn div_elem(&mut self, row: usize, col: usize, coef: f64) -> Result<()>;

    fn get_row(&self, index: usize) -> Option<&Vec<f64>>;

    fn get_row_as_matrix(&self, index: usize) -> Option<Self>
    where
        Self: std::marker::Sized;

    fn get_col(&self, index: usize) -> Option<Vec<f64>>;

    fn get_col_as_matrix(&self, index: usize) -> Option<Self>
    where
        Self: std::marker::Sized;

    fn scale(&self, scalar: f64) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn swap_elems_inplace(
        &mut self,
        row_a: usize,
        col_a: usize,
        row_b: usize,
        col_b: usize,
    ) -> Result<()>;

    fn swap_columns_inplace(&mut self, col_a: usize, col_b: usize) -> Result<()>;

    fn swap_rows_inplace(&mut self, row_a: usize, row_b: usize) -> Result<()>;
}

#[derive(Clone, Debug)]
pub struct RealMat {
    vals: Vec<Vec<f64>>,

    rows: usize,

    cols: usize,
}

impl RealMat {
    pub fn from_vec(v: Vec<Vec<f64>>) -> Result<RealMat> {
        let rs = v.len();
        let cs = v[0].len();

        for r in v.iter() {
            if r.len() != cs {
                return Err(MathError::DimensionError);
            }
        }

        let mat = RealMat {
            vals: v,
            rows: rs,
            cols: cs,
        };
        Ok(mat)
    }

    pub fn from_dims(rows: usize, cols: usize) -> RealMat {
        let mut vals = Vec::new();
        for _ in 0..rows {
            vals.push(vec![0.0; cols])
        }
        RealMat::from_vec(vals).unwrap()
    }

    /// Creates a diagonal matrix of dimensions n x n,
    /// with elements e on the diagonal.
    pub fn diag(e: f64, n: usize) -> RealMat {
        let mut res = RealMat::from_dims(n, n);
        for i in 0..n {
            res.set_elem(i, i, e).unwrap();
        }
        res
    }

    /// Creates an identity matrix of dimensions n x n.
    pub fn id(n: usize) -> RealMat {
        RealMat::diag(1.0, n)
    }

    /// Create a matrix from a file.
    pub fn from_file(path: &str) -> Result<RealMat> {
        let mut file = File::open(path).expect("Error while opening file, check path.");
        let mut contents = String::new();
        file.read_to_string(&mut contents)
            .expect("Unable to read from file.");

        let mut vals = Vec::new();
        for row in contents.split('\n') {
            let mut rvals = Vec::new();
            for col in row.split(' ') {
                let value = col.parse::<f64>().unwrap();
                rvals.push(value);
            }
            vals.push(rvals);
        }
        let mat = RealMat::from_vec(vals).unwrap();
        Ok(mat)
    }

    /// Write a matrix to a file.
    pub fn to_file(&self, path: &str) -> Result<()> {
        let mut file = File::create(path).expect("Error while opening file, check path.");
        let mut out = String::new();
        for i in 0..self.rows {
            for j in 0..self.cols {
                let e = self.get_elem(i, j).unwrap();
                out.push_str(&e.to_string());
                out.push(' ');
            }
            let _ = out.pop();
            out.push('\n');
        }
        file.write(out.as_bytes())
            .expect("Unable to write to file.");
        Ok(())
    }

    pub fn check_dims_add(&self, other: &RealMat) -> Result<()> {
        if self.cols != other.dim_cols() || self.rows != other.dim_rows() {
            return Err(MathError::DimensionError);
        }
        Ok(())
    }

    pub fn check_dims_mul(&self, other: &RealMat) -> Result<()> {
        if self.cols == other.rows {
            return Ok(());
        }
        Err(MathError::DimensionError)
    }

    /// Checks whether the element given by indices is zero.
    pub fn elem_zero(&self, row: usize, col: usize) -> bool {
        let e = self
            .get_elem(row, col)
            .expect("Invalid element index when checking for zero.");
        e.abs() <= PRECISION
    }
}

impl PartialEq for RealMat {
    fn eq(&self, other: &RealMat) -> bool {
        if self.check_dims_add(other).is_err() {
            return false;
        }
        let nrows = self.dim_rows();
        let ncols = self.dim_cols();

        for i in 0..nrows {
            for j in 0..ncols {
                let a = self.get_elem(i, j).unwrap();
                let b = self.get_elem(i, j).unwrap();
                if (a - b).abs() > PRECISION {
                    return false;
                }
            }
        }
        true
    }
}

impl Eq for RealMat {}

impl fmt::Display for RealMat {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", &self.vals[..])
    }
}

impl Matrix for RealMat {
    fn values(&self) -> &Vec<Vec<f64>> {
        &self.vals
    }

    fn values_mut(&mut self) -> &mut Vec<Vec<f64>> {
        &mut self.vals
    }

    fn dim_cols(&self) -> usize {
        self.cols
    }

    fn dim_rows(&self) -> usize {
        self.rows
    }

    fn is_square(&self) -> bool {
        self.rows == self.cols
    }

    fn get_elem(&self, row: usize, col: usize) -> Result<f64> {
        if row >= self.dim_rows() || col >= self.dim_cols() {
            return Err(MathError::InvalidIndexError);
        }
        Ok(self.vals.get(row).unwrap().get(col).unwrap().clone())
    }

    fn set_elem(&mut self, row: usize, col: usize, new: f64) -> Result<()> {
        let rower = self
            .vals
            .get_mut(row)
            .expect("Row index out of bounds while setting element.");
        let coler = rower
            .get_mut(col)
            .expect("Column index out of bounds while setting element.");
        *coler = new;
        Ok(())
    }

    fn mul_elem(&mut self, row: usize, col: usize, coef: f64) -> Result<()> {
        let elem = self.get_elem(row, col)?;
        let new = elem * coef;
        self.set_elem(row, col, new)
    }

    fn div_elem(&mut self, row: usize, col: usize, coef: f64) -> Result<()> {
        self.mul_elem(row, col, 1.0 / coef)
    }

    fn add_elem(&mut self, row: usize, col: usize, num: f64) -> Result<()> {
        let elem = self.get_elem(row, col)?;
        let new = elem + num;
        self.set_elem(row, col, new)
    }

    fn sub_elem(&mut self, row: usize, col: usize, num: f64) -> Result<()> {
        let elem = self.get_elem(row, col)?;
        let new = elem - num;
        self.set_elem(row, col, new)
    }

    fn get_row(&self, index: usize) -> Option<&Vec<f64>> {
        Some(self.vals.get(index)?)
    }

    /// Returns a matrix row as a new matrix.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0]
    ///     ]
    /// ).unwrap();
    /// let res = a.get_row_as_matrix(0).unwrap();
    /// assert_eq!(res, expected);
    /// ```
    fn get_row_as_matrix(&self, index: usize) -> Option<RealMat> {
        let row = self.get_row(index).expect("Index out of range.");
        let vec = RealVec::from_vec(row.clone());
        let mat = RealMat::from_vec_into_row(&vec);
        Some(mat.unwrap())
    }

    fn get_col(&self, index: usize) -> Option<Vec<f64>> {
        if index > self.cols {
            return None;
        }
        let mut col = Vec::with_capacity(self.rows);
        for r in self.vals.iter() {
            col.push(r.get(index)?.clone());
        }
        Some(col)
    }

    /// Returns a matrix column as a new matrix.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![2.0],
    ///         vec![5.0]
    ///     ]
    /// ).unwrap();
    /// let res = a.get_col_as_matrix(1).unwrap();
    /// assert_eq!(res, expected);
    /// ```
    fn get_col_as_matrix(&self, index: usize) -> Option<RealMat> {
        let col = self.get_col(index).expect("Invalid column index.");
        let vec = RealVec::from_vec(col);
        let res = RealMat::from_vec_into_col(&vec);
        Some(res.unwrap())
    }

    /// Swaps the positions of the two elements given by argument indices.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let mut a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 6.0, 3.0],
    ///         vec![4.0, 5.0, 2.0]
    ///     ]
    /// ).unwrap();
    /// a.swap_elems_inplace(0, 1, 1, 2).unwrap();
    /// assert_eq!(a, expected);
    /// ```
    fn swap_elems_inplace(
        &mut self,
        row_a: usize,
        col_a: usize,
        row_b: usize,
        col_b: usize,
    ) -> Result<()> {
        let temp = self.get_elem(row_a, col_a)?;
        let new = self.get_elem(row_b, col_b)?;

        self.set_elem(row_a, col_a, new)?;
        self.set_elem(row_b, col_b, temp)?;
        Ok(())
    }

    /// Swaps the corresponding elements of the two columns given by argument indices.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let mut a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 3.0, 2.0],
    ///         vec![4.0, 6.0, 5.0]
    ///     ]
    /// ).unwrap();
    /// a.swap_columns_inplace(1, 2).unwrap();
    /// assert_eq!(a, expected);
    /// ```
    fn swap_columns_inplace(&mut self, col_a: usize, col_b: usize) -> Result<()> {
        for row in 0..self.rows {
            self.swap_elems_inplace(row, col_a, row, col_b)?;
        }
        Ok(())
    }

    /// Swaps the corresponding elements of the two rows given by argument indices.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let mut a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![4.0, 5.0, 6.0],
    ///         vec![1.0, 2.0, 3.0]
    ///     ]
    /// ).unwrap();
    /// a.swap_rows_inplace(0, 1).unwrap();
    /// assert_eq!(a, expected);
    /// ```
    fn swap_rows_inplace(&mut self, row_a: usize, row_b: usize) -> Result<()> {
        for i in 0..self.cols {
            self.swap_elems_inplace(row_a, i, row_b, i)?;
        }
        Ok(())
    }

    /// Adds two matrices if their dimensions are compatible.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let b = RealMat::from_vec(
    ///     vec![
    ///         vec![2.0, 3.0, 4.0],
    ///         vec![5.0, 6.0, 7.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![3.0, 5.0, 7.0],
    ///         vec![9.0, 11.0, 13.0]
    ///     ]
    /// ).unwrap();
    ///
    /// let sum = a.add(&b).unwrap();
    /// assert_eq!(sum, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if the matrix dimensions are not compatible.
    fn add(&self, other: &RealMat) -> Result<RealMat> {
        self.check_dims_add(other)?;
        let n_rows = self.dim_rows();
        let n_cols = self.dim_cols();
        let mut res_rows = Vec::new();

        for i in 0..n_rows {
            let mut row = Vec::new();
            for j in 0..n_cols {
                let elem = self.get_elem(i, j).unwrap() + other.get_elem(i, j).unwrap();
                row.push(elem);
            }
            res_rows.push(row);
        }
        RealMat::from_vec(res_rows)
    }

    /// Adds two matrices if their dimensions are compatible.
    /// This operation stores the result in the first matrix.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let mut a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let b = RealMat::from_vec(
    ///     vec![
    ///         vec![2.0, 3.0, 4.0],
    ///         vec![5.0, 6.0, 7.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![3.0, 5.0, 7.0],
    ///         vec![9.0, 11.0, 13.0]
    ///     ]
    /// ).unwrap();
    ///
    /// a.add_inplace(&b).unwrap();
    /// assert_eq!(a, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if the matrix dimensions are not compatible.
    fn add_inplace(&mut self, other: &RealMat) -> Result<()> {
        self.check_dims_add(other)?;
        let n_rows = self.dim_rows();
        let n_cols = self.dim_cols();

        for i in 0..n_rows {
            for j in 0..n_cols {
                let elem = self.get_elem(i, j).unwrap() + other.get_elem(i, j).unwrap();
                self.set_elem(i, j, elem)?;
            }
        }
        Ok(())
    }

    /// Subtracts two matrices if their dimensions are compatible.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let b = RealMat::from_vec(
    ///     vec![
    ///         vec![2.0, 3.0, 4.0],
    ///         vec![5.0, 6.0, 7.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![-1.0, -1.0, -1.0],
    ///         vec![-1.0, -1.0, -1.0]
    ///     ]
    /// ).unwrap();
    ///
    /// let sum = a.sub(&b).unwrap();
    /// assert_eq!(sum, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if the matrix dimensions are not compatible.
    fn sub(&self, other: &RealMat) -> Result<RealMat> {
        self.check_dims_add(other)?;
        let n_rows = self.dim_rows();
        let n_cols = self.dim_cols();
        let mut res_rows = Vec::new();

        for i in 0..n_rows {
            let mut row = Vec::new();
            for j in 0..n_cols {
                let elem = self.get_elem(i, j).unwrap() - other.get_elem(i, j).unwrap();
                row.push(elem);
            }
            res_rows.push(row);
        }
        RealMat::from_vec(res_rows)
    }

    /// Subtracts two matrices if their dimensions are compatible.
    /// This operation stores the result in the first matrix.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let mut a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let b = RealMat::from_vec(
    ///     vec![
    ///         vec![2.0, 3.0, 4.0],
    ///         vec![5.0, 6.0, 7.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![-1.0, -1.0, -1.0],
    ///         vec![-1.0, -1.0, -1.0]
    ///     ]
    /// ).unwrap();
    ///
    /// a.sub_inplace(&b).unwrap();
    /// assert_eq!(a, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if the matrix dimensions are not compatible.
    fn sub_inplace(&mut self, other: &RealMat) -> Result<()> {
        self.check_dims_add(other)?;
        let n_rows = self.dim_rows();
        let n_cols = self.dim_cols();

        for i in 0..n_rows {
            for j in 0..n_cols {
                let elem = self.get_elem(i, j).unwrap() - other.get_elem(i, j).unwrap();
                self.set_elem(i, j, elem)?;
            }
        }
        Ok(())
    }

    /// Multiplies two matrices if their dimensions are compatible.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let b = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 4.0],
    ///         vec![2.0, 5.0],
    ///         vec![3.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![14.0, 32.0],
    ///         vec![32.0, 77.0]
    ///     ]
    /// ).unwrap();
    ///
    /// let prod = a.mul(&b).unwrap();
    /// assert!(b.mul(&a).is_ok());
    ///
    /// assert_eq!(prod.get_elem(0, 0).unwrap(), expected.get_elem(0, 0).unwrap());
    /// assert_eq!(prod.get_elem(0, 1).unwrap(), expected.get_elem(0, 1).unwrap());
    /// assert_eq!(prod.get_elem(1, 0).unwrap(), expected.get_elem(1, 0).unwrap());
    /// assert_eq!(prod.get_elem(1, 1).unwrap(), expected.get_elem(1, 1).unwrap());
    /// ```
    ///
    /// # Errors
    /// Returns an error if the matrix dimensions are not compatible.
    fn mul(&self, other: &RealMat) -> Result<RealMat> {
        self.check_dims_mul(other)?;
        let n = self.rows;
        let p = other.dim_cols();
        let m = self.cols;
        let mut res = Vec::with_capacity(n);

        for i in 0..n {
            let mut c = Vec::with_capacity(p);
            for j in 0..p {
                let mut sum = 0.0;
                for k in 0..m {
                    sum += self.get_elem(i, k).unwrap() * other.get_elem(k, j).unwrap();
                }
                c.push(sum);
            }
            res.push(c);
        }
        RealMat::from_vec(res)
    }

    /// Transposes a matrix.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 4.0],
    ///         vec![2.0, 5.0],
    ///         vec![3.0, 6.0]
    ///     ]
    /// ).unwrap();
    ///
    /// let t = a.transpose().unwrap();
    /// assert_eq!(t, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if matrix creation fails, which should be never.
    fn transpose(&self) -> Result<RealMat> {
        let n_cols = self.dim_rows();
        let n_rows = self.dim_cols();
        let mut rows = Vec::with_capacity(n_rows);

        for i in 0..n_rows {
            let mut row = Vec::with_capacity(n_cols);
            for j in 0..n_cols {
                let elem = self.get_elem(j, i).unwrap();
                row.push(elem.clone());
            }
            rows.push(row);
        }
        RealMat::from_vec(rows)
    }

    /// Creates a row matrix from a vector.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::{Vector, RealVec};
    /// use linalg::matrix::{Matrix, RealMat};
    ///
    /// let a = RealVec::from_vec(vec![12.0, 20.0, 8.0, 8.0]);
    /// let m = RealMat::from_vec_into_col(&a).unwrap();
    ///
    /// assert_eq!(m.dim_cols(), 1);
    /// assert_eq!(m.dim_rows(), 4);
    /// ```
    fn from_vec_into_col(v: &impl Vector) -> Result<RealMat> {
        let vals = v.elems();
        let mut rows = Vec::with_capacity(vals.len());
        for item in vals.iter() {
            rows.push(vec![item.clone()]);
        }
        RealMat::from_vec(rows)
    }

    /// Creates a row matrix from a vector.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::{Vector, RealVec};
    /// use linalg::matrix::{Matrix, RealMat};
    ///
    /// let a = RealVec::from_vec(vec![12.0, 20.0, 8.0, 8.0]);
    /// let m = RealMat::from_vec_into_row(&a).unwrap();
    ///
    /// assert_eq!(m.dim_rows(), 1);
    /// assert_eq!(m.dim_cols(), 4);
    /// ```
    fn from_vec_into_row(v: &impl Vector) -> Result<RealMat> {
        let cols = v.elems().iter().cloned().collect::<Vec<f64>>();
        let mut row = Vec::new();
        row.push(cols);
        RealMat::from_vec(row)
    }

    /// Multiplies all elements of a matrix with a scalar.panic!
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    /// let a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![2.0, 4.0, 6.0],
    ///         vec![8.0, 10.0, 12.0]
    ///     ]
    /// ).unwrap();
    /// let res = a.scale(2.0).unwrap();
    /// assert_eq!(res, expected);
    /// ```
    fn scale(&self, scalar: f64) -> Result<RealMat> {
        let n_rows = self.dim_rows();
        let n_cols = self.dim_cols();
        let mut res_rows = Vec::new();

        for i in 0..n_rows {
            let mut row = Vec::new();
            for j in 0..n_cols {
                let elem = self.get_elem(i, j).unwrap() * scalar;
                row.push(elem);
            }
            res_rows.push(row);
        }
        RealMat::from_vec(res_rows)
    }
}

/// Adds two matrices if their dimensions are compatible.
///
/// # Examples
/// ```
/// use linalg::matrix::Matrix;
/// use linalg::matrix::RealMat;
///
/// let a = RealMat::from_vec(
///     vec![
///         vec![1.0, 2.0, 3.0],
///         vec![4.0, 5.0, 6.0]
///     ]
/// ).unwrap();
/// let b = RealMat::from_vec(
///     vec![
///         vec![2.0, 3.0, 4.0],
///         vec![5.0, 6.0, 7.0]
///     ]
/// ).unwrap();
/// let expected = RealMat::from_vec(
///     vec![
///         vec![3.0, 5.0, 7.0],
///         vec![9.0, 11.0, 13.0]
///     ]
/// ).unwrap();
///
/// let sum = &a + &b;
/// assert_eq!(sum, expected);
/// ```
///
/// # Errors
/// Returns an error if the matrix dimensions are not compatible.
impl<'a, 'b> Add<&'b RealMat> for &'a RealMat {
    type Output = RealMat;

    fn add(self, other: &'b RealMat) -> RealMat {
        Matrix::add(self, other).unwrap()
    }
}

/// Adds two matrices if their dimensions are compatible.
///
/// # Examples
/// ```
/// use linalg::matrix::Matrix;
/// use linalg::matrix::RealMat;
///
/// let a = RealMat::from_vec(
///     vec![
///         vec![1.0, 2.0, 3.0],
///         vec![4.0, 5.0, 6.0]
///     ]
/// ).unwrap();
/// let b = RealMat::from_vec(
///     vec![
///         vec![2.0, 3.0, 4.0],
///         vec![5.0, 6.0, 7.0]
///     ]
/// ).unwrap();
/// let expected = RealMat::from_vec(
///     vec![
///         vec![-1.0, -1.0, -1.0],
///         vec![-1.0, -1.0, -1.0]
///     ]
/// ).unwrap();
///
/// let sum = &a - &b;
/// assert_eq!(sum, expected);
/// ```
///
/// # Errors
/// Returns an error if the matrix dimensions are not compatible.
impl<'a, 'b> Sub<&'b RealMat> for &'a RealMat {
    type Output = RealMat;

    fn sub(self, other: &'b RealMat) -> RealMat {
        Matrix::sub(self, other).unwrap()
    }
}

/// Multiplies two matrices if their dimensions are compatible.
///
/// # Examples
/// ```
/// use linalg::matrix::Matrix;
/// use linalg::matrix::RealMat;
///
/// let a = RealMat::from_vec(
///     vec![
///         vec![1.0, 2.0, 3.0],
///         vec![4.0, 5.0, 6.0]
///     ]
/// ).unwrap();
/// let b = RealMat::from_vec(
///     vec![
///         vec![1.0, 4.0],
///         vec![2.0, 5.0],
///         vec![3.0, 6.0]
///     ]
/// ).unwrap();
/// let expected = RealMat::from_vec(
///     vec![
///         vec![14.0, 32.0],
///         vec![32.0, 77.0]
///     ]
/// ).unwrap();
///
/// let prod = a.mul(&b).unwrap();
/// assert!(b.mul(&a).is_ok());
///
/// assert_eq!(prod.get_elem(0, 0).unwrap(), expected.get_elem(0, 0).unwrap());
/// assert_eq!(prod.get_elem(0, 1).unwrap(), expected.get_elem(0, 1).unwrap());
/// assert_eq!(prod.get_elem(1, 0).unwrap(), expected.get_elem(1, 0).unwrap());
/// assert_eq!(prod.get_elem(1, 1).unwrap(), expected.get_elem(1, 1).unwrap());
/// ```
///
/// # Errors
/// Returns an error if the matrix dimensions are not compatible.
impl<'a, 'b> Mul<&'b RealMat> for &'a RealMat {
    type Output = RealMat;

    fn mul(self, other: &'b RealMat) -> RealMat {
        Matrix::mul(self, other).unwrap()
    }
}
