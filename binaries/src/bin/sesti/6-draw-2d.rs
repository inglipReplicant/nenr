extern crate nnet;

use std::fs::File;
use std::io::Write;
use std::str::from_utf8;

use nnet::anfis::ANFIS;
use nnet::util::{create_anfis_examples, sigmoid_param};

fn main() {
    write_activation_functions();
    write_errors();
    //train_anfis(5, 100000, 81, 0.0005, false, false);
}

fn write_errors() {
    for learning_rate in [1e-7, 0.001, 0.1].iter() {
        for batch_size in [1, 81].iter() {
            let (_, errors) = train_anfis(5, 100000, *batch_size, *learning_rate, true, true);
            let errors = errors.unwrap();

            let rate = if eqls(*learning_rate, 1e-7) {
                "verylow"
            } else if eqls(*learning_rate, 0.001) {
                "fitting"
            } else {
                "veryhigh"
            };
            let size = match batch_size {
                1 => "stochastic",
                81 => "batch",
                _ => "other",
            };

            let mut path = Vec::new();
            writeln!(&mut path, "reports/anfis/2d/2d_error_{}_{}.txt", rate, size).unwrap();
            path.pop().unwrap();
            let path_str_a = from_utf8(&path).expect("Unable to convert path to string.");
            let mut plot_a = File::create(&path_str_a).expect("Unable to open 2d error plot file.");

            for (i, err) in (1..=errors.len()).zip(errors.iter()) {
                let mut out = Vec::new();
                writeln!(&mut out, "{} {}", i, err).unwrap();
                plot_a
                    .write(&out)
                    .expect("Unable to write to 2d error plot file.");
            }
        }
    }
}

fn eqls(one: f64, other: f64) -> bool {
    (one - other).abs() < 1e-6
}

fn write_activation_functions() {
    let (net, _) = train_anfis(5, 100000, 1, 0.001, false, true);

    for i in 1..=5 {
        let mut path_a = Vec::new();
        writeln!(&mut path_a, "reports/anfis/2d_sigm/2d_sigmoid_a{}.txt", i).unwrap();
        path_a.pop().unwrap();
        let path_str_a = from_utf8(&path_a).expect("Unable to convert path to string.");
        let mut plot_a = File::create(&path_str_a).expect("Unable to open sigmoid plot file.");

        let mut path_b = Vec::new();
        writeln!(&mut path_b, "reports/anfis/2d_sigm/2d_sigmoid_b{}.txt", i).unwrap();
        path_b.pop().unwrap();
        let path_str_b = from_utf8(&path_b).expect("Unable to convert path to string.");
        let mut plot_b = File::create(&path_str_b).expect("Unable to open sigmoid plot file.");

        for pt in -10..=10 {
            let x = pt as f64;
            let a_of_x = sigmoid_param(x, net.a[i - 1], net.b[i - 1]);
            let b_of_x = sigmoid_param(x, net.c[i - 1], net.d[i - 1]);

            let mut points = Vec::new();
            writeln!(&mut points, "{} {}", x, a_of_x).unwrap();
            plot_a
                .write(&points)
                .expect("Unable to write points to file sigm_a");

            let mut points = Vec::new();
            writeln!(&mut points, "{} {}", x, b_of_x).unwrap();
            plot_b
                .write(&points)
                .expect("Unable to write points to file sigm_b");
        }
    }
}

fn train_anfis(
    k: usize,
    iters: i32,
    batch_size: usize,
    learning_rate: f64,
    error_trace: bool,
    print_iters: bool,
) -> (ANFIS, Option<Vec<f64>>) {
    let dataset = create_anfis_examples();
    let mut net = ANFIS::new(dataset, k);

    let mut errs = Vec::new();
    for i in -1..iters {
        let _ = net.epoch(Some(batch_size), learning_rate, error_trace);
        if error_trace {
            errs.push(net.total_mse());
        }
        if print_iters && ((i + 1) % 10000 == 0) {
            println!("Iter {} - total MSE {}", i + 1, net.total_mse());
        }
    }
    if error_trace {
        return (net, Some(errs));
    }
    (net, None)
}
