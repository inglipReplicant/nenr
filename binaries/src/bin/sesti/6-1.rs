extern crate nnet;

use nnet::anfis::ANFIS;
use nnet::util::{anfis_function, create_anfis_examples};

fn main() {
    let dataset = create_anfis_examples();
    let mut net = ANFIS::new(dataset, 5);
    let iters = 500000;

    for i in -1..iters {
        let _ = net.epoch(Some(22), 0.001, false);
        if (i + 1) % 1000 == 0 {
            let x = (vec![-2.5, 2.5], anfis_function(-2.5, 2.5));
            println!("Iter {} - total MSE {}", i + 1, net.total_mse());
            println!(
                "Predicting {:?} ... {} (expected {})",
                x.0,
                net.forward_propagate(&x),
                anfis_function(-2.5, 2.5)
            );
            println!("");
        }
    }
}
