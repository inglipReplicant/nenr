extern crate evo;
extern crate nnet;

use evo::fitness::FitnessFunction;
use evo::ops::*;
use evo::population::Population;
use evo::real::*;
use evo::unit::Unit;

use rand::{thread_rng, Rng};

use nnet::evolnet::*;
use nnet::util::create_evolnet_dataset;

fn main() {
    let dataset = create_evolnet_dataset("binaries/files/zad7-dataset.txt");
    let mut evonet = EvolNet::new(vec![2, 8, 4, 3], dataset);

    let pop_size = 50;
    let prob_dimension = evonet.get_params().len();
    let mut_prob = 0.1;
    let mut_strength = 0.01;
    let _elim_coef = 7;
    let alpha = 0.3;
    let max_generations = 1000000;
    let num_parents = 3;
    let err_margin = 1e-7;

    let best = genetic(
        &mut evonet,
        pop_size,
        prob_dimension,
        mut_prob,
        mut_strength,
        _elim_coef,
        alpha,
        max_generations,
        num_parents,
        err_margin,
    )
    .unwrap();
    evonet.set_params(best.get_chromo());
    let dataset = create_evolnet_dataset("binaries/files/zad7-dataset.txt");

    let mut correct = 0;
    for ex in dataset.iter() {
        let mut pred = evonet.predict(&ex.0);
        for i in 0..pred.len() {
            pred[i] = if pred[i] >= 0.5 { 1.0 } else { 0.0 };
        }
        let mut eq = true;
        for i in 0..pred.len() {
            if pred[i] != ex.1[i] {
                eq = false;
            }
        }
        if eq {
            correct += 1;
        }
        //println!("Predicted: {:?}   |   Expected: {:?}", pred, ex.1);
    }
    println!(
        "Correctly classified {}% examples.",
        (correct as f64 / dataset.len() as f64) * 100.0
    );
}

fn genetic(
    fun: &mut EvolNet,
    pop_size: usize,
    prob_dimension: usize,
    mut_prob: f64,
    mut_strength: f64,
    _elim_coef: usize,
    alpha: f64,
    max_generations: usize,
    num_parents: usize,
    err_margin: f64,
) -> Option<RealUnit> {
    let mut rng = thread_rng();
    let mut fun = fun.clone();

    let selector = TourneySelect::new(num_parents);
    let breeder = BlxAlpha { alpha: alpha };
    let mutator = RealMutator {
        mutation_probability: mut_prob,
        mutation_strength: mut_strength,
    };

    let mut result = None;
    let mut best_fit = fun.fitness_max();
    let mut pop = generate_initial_genetic(pop_size, prob_dimension, &mut fun);

    fun.update_evaluate_real_pop(&mut pop);
    pop.sort_ascending();
    for i in 0..max_generations {
        let best = pop.units().get(0).unwrap();
        result = Some(best.clone());

        if (i + 1) % 1000 == 0 {
            println!("Iteration {}: best {}", i + 1, best.get_fitness_scalar());
        }
        if best.get_fitness_scalar() < best_fit {
            best_fit = best.get_fitness_scalar();
        } else if best.get_fitness_scalar() < err_margin {
            return result;
        }

        let three_parents = selector.select_from(&pop, &mut rng);
        let mut parent_pop = Population::from_units(three_parents);
        fun.update_evaluate_real_pop(&mut parent_pop);
        parent_pop.sort_ascending();

        let child;
        {
            let p1 = parent_pop.units().get(0).unwrap();
            let p2 = parent_pop.units().get(1).unwrap();
            child = breeder.crossover(vec![p1.clone(), p2.clone()], &mut rng)[0].clone();
        }
        let mut child = mutator.mutate(child, &mut rng);
        let fit = fun.fitness_of(&child);
        child.update_fitness(fit);

        let third = parent_pop.units().get(2).unwrap();
        if child.get_fitness_scalar() < third.get_fitness_scalar() {
            for unit in pop.units_mut().iter_mut() {
                if (unit.get_fitness_scalar() - third.get_fitness_scalar()).abs() < err_margin {
                    unit.update_chromosome(child.get_chromo().clone());
                    unit.update_fitness(*child.get_fitness());
                }
            }
        }
    }
    return result;
}

pub fn generate_initial_genetic(
    size: usize,
    solution_dim: usize,
    evaluator: &mut EvolNet,
) -> Population<RealUnit> {
    let mut units = Vec::new();
    let mut rng = thread_rng();
    for _ in 0..size {
        let mut chromos = Vec::new();
        for _ in 0..solution_dim {
            chromos.push(rng.gen_range(-5.0, 5.0));
        }
        let mut unit = RealUnit::from_vec(chromos);
        let fit = evaluator.fitness_of(&unit);
        unit.update_fitness(fit);
        units.push(unit);
    }
    Population::from_units(units)
}
