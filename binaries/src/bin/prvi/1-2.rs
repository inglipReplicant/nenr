extern crate fuzzy;

use fuzzy::domain::Element;
use fuzzy::domain::SimpleDomain;
use fuzzy::fset::{CalcFset, MutableFset};
//use fuzzy::ops::{IntUnaryFn};
use fuzzy::ops::unary_ops::{GammaFun, LFun, LambdaFun};
use fuzzy::util::print_set;

fn main() {
    let d1 = SimpleDomain::new(0, 11).unwrap();
    let mut s1 = MutableFset::new(d1);
    s1.set_value(&Element::from_vec(vec![0]), 1.0).unwrap();
    s1.set_value(&Element::from_vec(vec![1]), 0.8).unwrap();
    s1.set_value(&Element::from_vec(vec![2]), 0.6).unwrap();
    s1.set_value(&Element::from_vec(vec![3]), 0.4).unwrap();
    s1.set_value(&Element::from_vec(vec![4]), 0.2).unwrap();
    print_set(&s1);

    let d2 = SimpleDomain::new(-5, 6).unwrap();
    let lfun = LFun::new(-4, 4);
    let s2 = CalcFset::new(d2.clone(), &lfun);

    let gfun = GammaFun::new(-4, 4);
    let s3 = CalcFset::new(d2, &gfun);

    let lamfun = LambdaFun::new(-4, 0, 4);
    let d3 = SimpleDomain::new(-5, 6).unwrap();
    let s4 = CalcFset::new(d3, &lamfun);

    let vector = vec![s2, s3, s4];
    for x in vector.iter() {
        print_set(x);
    }
}
