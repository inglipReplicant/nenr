extern crate fuzzy;

use fuzzy::domain::SimpleDomain;
use fuzzy::domain::{Domain, Element};
use fuzzy::util::print_domain;

fn main() {
    let d1: SimpleDomain = SimpleDomain::new(0, 5).unwrap();
    print_domain(d1.clone());

    let d2 = SimpleDomain::new(0, 3).unwrap();
    print_domain(d2.clone());

    let d3 = Domain::combine(&d1, &d2);
    println!("{:?}", d3);
    print_domain(d3.clone());

    println!("{}", d3.elem_get(0).unwrap());
    println!("{}", d3.elem_get(5).unwrap());
    println!("{}", d3.elem_get(14).unwrap());
    println!("{}", d3.elem_index(&Element::from_vec(vec![4, 1])).unwrap());
}
