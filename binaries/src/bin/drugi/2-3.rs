use fuzzy::domain::*;
use fuzzy::errors::FuzzyError;
use fuzzy::fset::*;
use fuzzy::relations::*;

type Result<T> = std::result::Result<T, FuzzyError>;

fn main() -> Result<()> {
    let u = SimpleDomain::new(1, 5)?;

    let mut r = MutableFset::new(Domain::combine(&u, &u));
    r.set_value(&Element::from_vec(vec![1, 1]), 1.0)?;
    r.set_value(&Element::from_vec(vec![2, 2]), 1.0)?;
    r.set_value(&Element::from_vec(vec![3, 3]), 1.0)?;
    r.set_value(&Element::from_vec(vec![4, 4]), 1.0)?;
    r.set_value(&Element::from_vec(vec![1, 2]), 0.3)?;
    r.set_value(&Element::from_vec(vec![2, 1]), 0.3)?;
    r.set_value(&Element::from_vec(vec![2, 3]), 0.5)?;
    r.set_value(&Element::from_vec(vec![3, 2]), 0.5)?;
    r.set_value(&Element::from_vec(vec![3, 4]), 0.2)?;
    r.set_value(&Element::from_vec(vec![4, 3]), 0.2)?;

    let mut res = r.clone();

    println!(
        "Pocetna relacija je neizrazita relacija ekvivalencije? {}",
        is_fuzzy_equivalence(&res)?
    );
    println!("");

    for i in 1..=3 {
        println!("Broj odradenih kompozicija: {}. Relacija je: ", i);
        res = compose_binary(&res, &r)?;
        for e in res.get_domain().clone() {
            println!("mu({})={}", e, res.get_val_at(&e).unwrap());
        }
        println!(
            "Ova relacija je neizrazita relacija ekvivalencije? {}",
            is_fuzzy_equivalence(&res)?
        );
        println!("");
    }

    Ok(())
}
