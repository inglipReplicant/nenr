extern crate evo;
extern crate rand;

use evo::fitness::FitnessFunction;
use evo::ops::*;
use evo::population::Population;
use evo::real::*;
use evo::unit::Unit;
use rand::thread_rng;

use std::fs::File;
use std::io::prelude::*;

fn main() {
    println!("Solving the problem with generative genetic algorithm.");

    let mut fun = create_function_from_file("zadaci/zad4-dataset1.txt");
    let pop_size = 50;
    let prob_dimension = 5;
    let mut_prob = 0.1;
    let mut_strength = 0.01;
    let elitism_coef = 3;
    //let _elim_coef = pop_size / 2;
    let alpha = 0.3;
    let max_generations = 100_000;
    let err_margin = 1e-9;

    let mut rng = thread_rng();

    let selector = RouletteSelect {};
    let breeder = BlxAlpha { alpha: alpha };
    let mutator = RealMutator {
        mutation_probability: mut_prob,
        mutation_strength: mut_strength,
    };

    let mut best_fit = fun.fitness_max();
    let mut pop = generate_initial_genetic(pop_size, prob_dimension, &mut fun);
    for i in 0..max_generations {
        fun.evaluate_update_population(&mut pop);
        pop.sort_ascending();
        let best = pop.units().get(0).unwrap();

        if best.get_fitness_scalar() < best_fit {
            best_fit = best.get_fitness_scalar();
            println!("Iteration {}, new best unit is {:?}", i, best);
        } else if best.get_fitness_scalar() < err_margin {
            println!("Found best in iter {}: {:?}", i, best);
            break;
        }

        let mut next_gen = Vec::new();
        for i in 0..elitism_coef {
            next_gen.push(pop.units().get(i).unwrap().clone());
        }

        pop.sort_normalize();
        //let sum = pop.fitness_sum();

        let new_units = pop_size - elitism_coef;
        for _ in 0..new_units {
            let fst_parent = selector.select_from(&pop, &mut rng)[0].clone();
            let snd_parent = selector.select_from(&pop, &mut rng)[0].clone();

            let child = breeder.crossover(vec![fst_parent, snd_parent], &mut rng);
            let child = mutator.mutate(child[0].clone(), &mut rng);
            next_gen.push(child);
        }
        pop = Population::from_units(next_gen);
    }
}

fn create_function_from_file(path: &str) -> LabosRegression {
    let mut file = File::open(path).expect("EEEEEEEK wrong path!");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Unable to read from file.");

    let mut xs = Vec::new();
    let mut ys = Vec::new();
    let mut zs = Vec::new();

    for line in contents.trim().split('\n') {
        let temp = line
            .trim()
            .split('\t')
            .map(|s| s.trim().parse::<f64>().unwrap())
            .collect::<Vec<f64>>();
        xs.push(temp[0]);
        ys.push(temp[1]);
        zs.push(temp[2]);
    }
    LabosRegression {
        xs: xs,
        ys: ys,
        zs: zs,
    }
}
