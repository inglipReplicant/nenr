extern crate linalg;
extern crate nnet;

use linalg::matrix::RealMat;
use nnet::layer::{LinearLayer, SigmoidLayer};
use nnet::net::NeuralNet;

fn main() {
    let pairs = vec![
        (-1.0, 1.0),
        (-0.8, 0.64),
        (-0.6, 0.36),
        (-0.4, 0.16),
        (-0.2, 0.04),
        (0.0, 0.0),
        (0.2, 0.04),
        (0.4, 0.16),
        (0.6, 0.36),
        (0.8, 0.64),
        (1.0, 1.0),
    ];
    let (xs, ys) = create_examples(&pairs);

    let mut layer1 = LinearLayer::new(1, 6);
    let mut sigm1 = SigmoidLayer::new(6);
    let mut layer2 = LinearLayer::new(6, 6);
    let mut sigm2 = SigmoidLayer::new(6);
    let mut layer3 = LinearLayer::new(6, 1);
    println!("Layer: {:?}\n=====================\n", layer1);
    println!("Layer: {:?}\n=====================\n", layer3);

    let mut net = NeuralNet::new(vec![
        &mut layer1,
        &mut sigm1,
        &mut layer2,
        &mut sigm2,
        &mut layer3,
    ]);
    net.fit(&xs[..], &ys[..], Some(20), 1e-6, 1000, 0.6);

    println!("\n\n");
    let test_x = RealMat::from_vec(vec![vec![0.2]]).unwrap();
    println!("Output for {}: {}", test_x, net.forward_prop_once(&test_x));
    let test_x = RealMat::from_vec(vec![vec![2.0]]).unwrap();
    println!("Output for {}: {}", test_x, net.forward_prop_once(&test_x));
    let test_x = RealMat::from_vec(vec![vec![0.5]]).unwrap();
    println!("Output for {}: {}", test_x, net.forward_prop_once(&test_x));
    let test_x = RealMat::from_vec(vec![vec![0.8]]).unwrap();
    println!("Output for {}: {}", test_x, net.forward_prop_once(&test_x));
    let test_x = RealMat::from_vec(vec![vec![0.0]]).unwrap();
    println!("Output for {}: {}", test_x, net.forward_prop_once(&test_x));
    let test_x = RealMat::from_vec(vec![vec![0.6]]).unwrap();
    println!("Output for {}: {}", test_x, net.forward_prop_once(&test_x));
}

fn create_examples(pairs: &[(f64, f64)]) -> (Vec<RealMat>, Vec<RealMat>) {
    let mut xs = Vec::new();
    let mut ys = Vec::new();
    for p in pairs {
        let x = p.0;
        let y = p.1;

        xs.push(RealMat::from_vec(vec![vec![x]]).unwrap());
        ys.push(RealMat::from_vec(vec![vec![y]]).unwrap());
    }
    (xs, ys)
}
