extern crate linalg;
extern crate nnet;

use linalg::matrix::RealMat;
use nnet::layer::{LinearLayer, SigmoidLayer};
use nnet::net::NeuralNet;

fn main() {
    let xs = vec![vec![1.0, 0.0, 0.0, 0.0]];
    let x = RealMat::from_vec(xs).unwrap();

    let ys = vec![vec![1.0, 0.0, 0.0, 0.0]];
    let y = RealMat::from_vec(ys).unwrap();

    let mut layer = LinearLayer::new(4, 20);
    let mut sig1 = SigmoidLayer::new(20);
    let mut layer2 = LinearLayer::new(20, 4);
    let mut sig2 = SigmoidLayer::new(4);

    let mut net = NeuralNet::new(vec![&mut layer, &mut sig1, &mut layer2, &mut sig2]);
    net.fit(
        &vec![x.clone(); 10],
        &vec![y.clone(); 10],
        Some(10),
        1e-6,
        100,
        0.1,
    );

    let ds = vec![vec![0.0, 0.0, 0.0, 10000.0]];
    let d = RealMat::from_vec(ds).unwrap();
    println!("{}", net.forward_prop_once(&d));
}
