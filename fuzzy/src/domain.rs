use crate::errors::FuzzyError;
use std::fmt;

type Result<T> = std::result::Result<T, FuzzyError>;

pub trait Domain: std::clone::Clone + std::iter::IntoIterator<Item = Element> {
    fn cardinality(&self) -> usize;

    fn comp(&self, ind: usize) -> Option<&SimpleDomain>;

    fn comp_count(&self) -> usize;

    fn elem_get(&self, index: usize) -> Option<Element> {
        self.clone().into_iter().nth(index)
    }

    fn elem_index(&self, elem: &Element) -> Option<usize> {
        let mut ctr = 0;
        for x in self.clone().into_iter() {
            if x == *elem {
                return Some(ctr);
            }
            ctr += 1;
        }
        None
    }

    fn from_range(start: i32, stop: i32) -> Result<SimpleDomain> {
        SimpleDomain::new(start, stop)
    }

    fn combine(fst: &Self, snd: &Self) -> CompositeDomain {
        let mut domains = Vec::new();
        for x in 0..fst.comp_count() {
            domains.push(fst.comp(x).unwrap().clone());
        }
        for x in 0..snd.comp_count() {
            domains.push(snd.comp(x).unwrap().clone());
        }
        CompositeDomain::from_domains(domains).unwrap()
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Element {
    vals: Vec<i32>,
}

impl Element {
    pub fn from_vec(values: Vec<i32>) -> Element {
        Element { vals: values }
    }

    pub fn comp_value(&self, i: usize) -> i32 {
        self.vals[i]
    }

    pub fn comp_count(&self) -> usize {
        1
    }

    pub fn to_string(&self) -> String {
        let mut s = String::new();
        s.push('(');
        for x in 0..self.vals.len() {
            s.push_str(&self.vals[x].to_string());
            s.push_str(", ");
        }
        s.pop();
        s.pop();
        s.push(')');
        s
    }
}

impl fmt::Display for Element {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

#[derive(Clone, Debug)]
pub struct SimpleDomain {
    pub start: i32,

    pub stop: i32,
}

impl SimpleDomain {
    pub fn new(start_: i32, stop_: i32) -> Result<SimpleDomain> {
        match start_ < stop_ {
            true => Ok(SimpleDomain {
                start: start_,
                stop: stop_,
            }),
            false => Err(FuzzyError::IntervalError),
        }
    }

    pub fn from_domain(other: &SimpleDomain) -> SimpleDomain {
        SimpleDomain::new(other.start, other.stop).unwrap()
    }
}

impl Domain for SimpleDomain {
    fn cardinality(&self) -> usize {
        ((self.stop - self.start).abs()) as usize
    }

    fn comp(&self, _: usize) -> Option<&SimpleDomain> {
        Some(&self)
    }

    fn comp_count(&self) -> usize {
        1
    }

    fn elem_get(&self, index: usize) -> Option<Element> {
        let index = index as i32;
        if index >= self.start && index < self.stop {
            return Some(Element::from_vec(vec![index]));
        }
        None
    }
}

impl IntoIterator for SimpleDomain {
    type Item = Element;
    type IntoIter = SimpleDomainIter;

    fn into_iter(self) -> Self::IntoIter {
        SimpleDomainIter {
            start: self.start,
            stop: self.stop,
            index: self.start,
        }
    }
}

#[derive(Clone)]
pub struct SimpleDomainIter {
    start: i32,
    stop: i32,
    index: i32,
}

impl Iterator for SimpleDomainIter {
    type Item = Element;

    fn next(&mut self) -> Option<Element> {
        if self.index >= self.start && self.index < self.stop {
            let element = Element::from_vec(vec![self.index]);
            self.index += 1;
            return Some(element);
        }
        None
    }
}

#[derive(Clone, Debug)]
pub struct CompositeDomain {
    components: Vec<SimpleDomain>,
}

impl CompositeDomain {
    pub fn from_domains(domains: Vec<SimpleDomain>) -> Option<CompositeDomain> {
        if domains.len() == 0 {
            return None;
        }
        Some(CompositeDomain {
            components: domains,
        })
    }
}

impl Domain for CompositeDomain {
    fn cardinality(&self) -> usize {
        self.components
            .iter()
            .fold(1, |acc, x| acc * x.cardinality())
    }

    fn comp(&self, index: usize) -> Option<&SimpleDomain> {
        self.components.get(index)
    }

    fn comp_count(&self) -> usize {
        self.components.len()
    }
}

impl IntoIterator for CompositeDomain {
    type Item = Element;
    type IntoIter = CompositeDomainIter;

    fn into_iter(self) -> Self::IntoIter {
        let mut counter = Vec::with_capacity(self.cardinality());
        for x in 0..self.components.len() {
            counter.push(self.comp(x).unwrap().start);
        }
        let card = self.cardinality();

        CompositeDomainIter {
            components: self.components,
            counter: counter,
            inner_ctr: 0,
            cardinality: card,
        }
    }
}

#[derive(Clone, Debug)]
pub struct CompositeDomainIter {
    components: Vec<SimpleDomain>,
    counter: Vec<i32>,
    inner_ctr: usize,
    cardinality: usize,
}

impl Iterator for CompositeDomainIter {
    type Item = Element;

    fn next(&mut self) -> Option<Self::Item> {
        if self.inner_ctr >= self.cardinality {
            return None;
        }
        let elem = Some(Element::from_vec(self.counter.clone()));

        let mut curr_comp_ind = self.components.len() - 1;
        let mut curr_comp: &SimpleDomain = self.components.get(curr_comp_ind).unwrap();
        let mut overflow = false;

        if self.counter[curr_comp_ind] < curr_comp.stop - 1 {
            self.counter[curr_comp_ind] += 1;
        } else {
            self.counter[curr_comp_ind] = curr_comp.start;
            overflow = true;
        }

        while overflow {
            if curr_comp_ind == 0 {
                break;
            }
            curr_comp_ind -= 1;
            curr_comp = self.components.get(curr_comp_ind).unwrap();

            if self.counter[curr_comp_ind] < curr_comp.stop - 1 {
                self.counter[curr_comp_ind] += 1;
                overflow = false;
            } else {
                self.counter[curr_comp_ind] = curr_comp.start;
            }
        }
        self.inner_ctr += 1;
        elem
    }
}
