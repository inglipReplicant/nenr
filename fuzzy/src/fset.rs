/// Basic operations on fuzzy sets.
use super::domain::{Domain, Element};
use super::errors::FuzzyError;
use super::ops::IntUnaryFn;

type Result<T> = std::result::Result<T, FuzzyError>;

pub trait FuzzySet<T: Domain> {
    /// Get the domain of the fuzzy set.
    fn get_domain(&self) -> &T;

    /// Get the value of a corresponding element.
    fn get_val_at(&self, elem: &Element) -> Result<f64>;
}

#[derive(Debug)]
/// CalcFset is a fuzzy set whose values are calculated on the fly.
/// Upon receiving the element, its value is calculated using `F`.
pub struct CalcFset<'a, T: Domain> {
    /// Domain of the fuzzy set.
    domain: T,

    /// Unary function which is the indicator function to be used on the corresponding elements.
    pub fun: &'a dyn IntUnaryFn,
}

impl<'a, T: Domain> CalcFset<'a, T> {
    pub fn new(domain_: T, fun_: &'a dyn IntUnaryFn) -> CalcFset<T> {
        CalcFset {
            domain: domain_,
            fun: fun_,
        }
    }

    /// Sets the indicator function to `fun_`.
    pub fn set_fn(&mut self, fun_: &'a dyn IntUnaryFn) {
        self.fun = fun_
    }
}

impl<'a, T: Domain> FuzzySet<T> for CalcFset<'a, T> {
    /// Returns the domain of the fuzzy set.
    fn get_domain(&self) -> &T {
        &self.domain
    }

    /// Calculates the value of `elem`.
    /// # Errors
    /// Returns an `IndexError` if the index is out of bounds.
    fn get_val_at(&self, elem: &Element) -> Result<f64> {
        let ind = self.domain.elem_index(elem);
        if ind.is_none() {
            return Err(FuzzyError::IndexError);
        }
        Ok(self.fun.value_at(elem.comp_value(0)))
    }
}

#[derive(Clone, Debug)]
/// `MutableFset` is a fuzzy set whose members' values are mutable.
/// These can be manually set using `set_value`.
pub struct MutableFset<T: Domain> {
    /// Vector of member values.
    memberships: Vec<f64>,

    /// Domain of the mutable fuzzy set.
    domain: T,
}

impl<T: Domain> MutableFset<T> {
    /// Creates a new MutableFset while consuming the domain.
    pub fn new(domain: T) -> MutableFset<T> {
        let mem = vec![0.0; domain.cardinality()];
        MutableFset {
            memberships: mem,
            domain: domain,
        }
    }

    /// Sets the value of the indicator function for the element `e`.
    pub fn set_value(&mut self, elem: &Element, value: f64) -> Result<()> {
        if value < 0.0 || value > 1.0 {
            return Err(FuzzyError::CodomainError);
        }
        let ind = self.domain.elem_index(elem).expect("Bad index.");
        self.memberships[ind] = value;
        Ok(())
    }
}

impl<T: Domain> FuzzySet<T> for MutableFset<T> {
    /// Returns the domain of the MutableFset.
    fn get_domain(&self) -> &T {
        &self.domain
    }

    /// Returns the value of the indicator function for the corresponding `elem`.
    fn get_val_at(&self, elem: &Element) -> Result<f64> {
        let ind = self.domain.elem_index(elem);
        let ind = match ind {
            Some(x) => x,
            None => return Err(FuzzyError::IndexError),
        };
        Ok(*self.memberships.get(ind).unwrap())
    }
}
