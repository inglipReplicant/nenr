use super::domain::Domain;
use super::fset::FuzzySet;

pub fn print_domain<T>(d: T)
where
    T: Domain,
{
    println!("Elementi domene: ");
    println!("Kardinalitet domene: {}", d.cardinality());
    for x in d {
        println!("{}", x);
    }
    println!("");
}

pub fn print_set<T: FuzzySet<D>, D: Domain>(s: &T) {
    println!("Printing set: ");
    let domain = s.get_domain();
    for x in domain.clone() {
        println!("d{}={:.6}", x, s.get_val_at(&x).unwrap())
    }
    println!("");
}
