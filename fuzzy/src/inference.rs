use super::domain::{Domain, Element};
use super::fset::{CalcFset, FuzzySet, MutableFset};
use super::ops::binary_ops::ZadehOr;
use super::ops::real_binary_operate;

pub mod decode {
    use super::*;

    pub fn center_of_area<T: Domain>(set: &MutableFset<T>) -> f64 {
        let mut numer = 0.0;
        let mut denom = 0.0;
        for x in set.get_domain().clone().into_iter() {
            let mu = set.get_val_at(&x).unwrap();
            denom += mu;
            numer += mu * x.comp_value(0) as f64;
        }
        numer / denom
    }
}

/// Single fuzzy rule.
pub struct Rule<'a, T: Domain> {
    premises: Vec<CalcFset<'a, T>>,

    conseq: CalcFset<'a, T>,
}

impl<'a, T: Domain> Rule<'a, T> {
    pub fn new(ps: Vec<CalcFset<'a, T>>, cons: CalcFset<'a, T>) -> Rule<'a, T> {
        Rule {
            premises: ps,
            conseq: cons,
        }
    }
}

/// Fuzzy controllers.
pub trait FuzzyControl {
    fn update_inputs(&mut self, xs: Vec<i32>);

    fn infer(&self, xs: &Vec<i32>) -> i32;
}

pub struct MinimumController<'a, T: Domain> {
    pub xs: Vec<i32>,

    pub rules: Vec<Rule<'a, T>>,
}

impl<'a, T: Domain> MinimumController<'a, T> {
    pub fn new(xs: Vec<i32>, rules: Vec<Rule<'a, T>>) -> MinimumController<'a, T> {
        MinimumController {
            xs: xs,
            rules: rules,
        }
    }
}

impl<'a, T: Domain> FuzzyControl for MinimumController<'a, T> {
    fn update_inputs(&mut self, xs: Vec<i32>) {
        self.xs = xs;
    }

    fn infer(&self, xs: &Vec<i32>) -> i32 {
        eprintln!("Begin!!! Infering for x: {:?}", xs);
        let mut ys = Vec::new();

        for rule in self.rules.iter() {
            let domain = rule.conseq.get_domain();
            let (premises, conseq) = (&rule.premises, &rule.conseq);
            let mut new = MutableFset::new(conseq.get_domain().clone());
            let min = xs.iter().zip(premises.iter()).fold(1.0, |acc, (x, p)| {
                let temp = p
                    .get_val_at(&Element::from_vec(vec![*x]))
                    .expect("Element not present in set?");
                eprintln!("Temp: {} with function {:?} for x {}", temp, p.fun, x);
                if temp < acc {
                    temp
                } else {
                    acc
                }
            });
            eprintln!("Minimum is {}", min);

            for y in domain.clone() {
                new.set_value(&y, min.min(conseq.get_val_at(&y).unwrap()))
                    .unwrap();
            }
            ys.push(new);
        }

        let domain = self.rules[0].conseq.get_domain();
        let mut conclusion = MutableFset::new(domain.clone());
        let or = ZadehOr {};
        for y in ys.iter() {
            conclusion = real_binary_operate(&conclusion, y, &or);
        }

        let res = decode::center_of_area(&conclusion);
        eprintln!("Result: {}", res);
        res as i32
    }
}

pub struct ProductController<'a, T: Domain> {
    pub xs: Vec<i32>,

    pub rules: Vec<Rule<'a, T>>,
}

impl<'a, T: Domain> ProductController<'a, T> {
    pub fn new(xs: Vec<i32>, rules: Vec<Rule<'a, T>>) -> ProductController<'a, T> {
        ProductController {
            xs: xs,
            rules: rules,
        }
    }
}

impl<'a, T: Domain> FuzzyControl for ProductController<'a, T> {
    fn update_inputs(&mut self, xs: Vec<i32>) {
        self.xs = xs;
    }

    fn infer(&self, xs: &Vec<i32>) -> i32 {
        eprintln!("Begin!!! Infering for x: {:?}", xs);
        let mut ys = Vec::new();

        for rule in self.rules.iter() {
            let domain = rule.conseq.get_domain();
            let (premises, conseq) = (&rule.premises, &rule.conseq);
            let mut new = MutableFset::new(conseq.get_domain().clone());
            let min = xs.iter().zip(premises.iter()).fold(1.0, |acc, (x, p)| {
                let temp = p.get_val_at(&Element::from_vec(vec![*x])).unwrap();
                eprintln!("Temp: {} with function {:?} for x {}", temp, p.fun, x);
                if temp < acc {
                    temp
                } else {
                    acc
                }
            });
            eprintln!("Minimum is {}", min);

            for y in domain.clone() {
                new.set_value(&y, min * conseq.get_val_at(&y).unwrap())
                    .unwrap();
            }
            ys.push(new);
        }

        let domain = self.rules[0].conseq.get_domain();
        let mut conclusion = MutableFset::new(domain.clone());
        let or = ZadehOr {};
        for y in ys.iter() {
            conclusion = real_binary_operate(&conclusion, y, &or);
        }

        let res = decode::center_of_area(&conclusion);
        eprintln!("Result: {}", res);
        res as i32
    }
}
