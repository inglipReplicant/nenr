import numpy as np
import sys
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def filter_by_class(xs, ys, y_ind):
    new_xs = []
    new_ys = []
    for i in range(len(xs)):
        if ys[i][y_ind] == 1:
            new_xs.append(xs[i])
            new_ys.append(ys[i])

    return np.array(new_xs), np.array(new_ys)

filename = sys.argv[1]
xs = []
ys = []

with open(filename, 'r') as in_file:
    for line in in_file.readlines():
        if line is not '\n':
            splitter = line.strip().split('\t')
            x = list(map(float, splitter[0:2]))
            y = list(map(float, splitter[2:]))
            xs.append(x)
            ys.append(y)

weightname = sys.argv[2]
ws = []
ss = []

with open(weightname, 'r') as in_file:
    for line in in_file.readlines():
        if line is not '\n':
            splitter = line.strip().split(' ')
            w = list(map(float, splitter[0:2]))
            s = list(map(float, splitter[2:]))
            ws.append(w)
            ss.append(s)

x_arr_1, y_arr_1 = filter_by_class(xs, ys, 0)
x_arr_2, y_arr_2 = filter_by_class(xs, ys, 1)
x_arr_3, y_arr_3 = filter_by_class(xs, ys, 2)

ws = np.array(ws)
ss = np.array(ss)

fig = plt.subplots(2, 1)
ax = plt.subplot(2, 1, 1)
ax.set(title=(weightname[:-4]))

ax.scatter(x_arr_1[:, 0], x_arr_1[:, 1], marker="o")
ax.scatter(x_arr_2[:, 0], x_arr_2[:, 1], marker="+")
ax.scatter(x_arr_3[:, 0], x_arr_3[:, 1], marker="x")
ax.scatter(ws[:, 0], ws[:, 1], marker="8")
ax.legend(["0", "1", "2", "w"])

ax = plt.subplot(2, 1, 2)
ax.scatter(ss[:, 0], ss[:, 1], marker="D")
ax.set(title=(weightname[:-4]))
ax.legend(["s"])

plt.savefig("img_" + weightname[:-4])
